# Lab5 -- Integration testing

## Introduction

We've covered unit testing, which is widely used in indusry and is completely whitebox, but there are times when you need to use blackbox testing, for example if you cannot access code module, or if you need to check propper work of module, while calling it locally, so it returns correct values. That's what integration testing is being used for, it can be used both for whitebox and blackbox testing, depending on your goals and possibilities.   ***Let's roll!***🚀️

## Integration testing

Well, if unit testing is all about atomicity and independency, integration testing doesn't think like it, it is used less often then unit testing because it requires more resourses, and I haven't met a team yet, where developers were doing integration tests, usually it is a task for the automated testing team, to check integration of one module with another. Main goal of integration testing is to check that two connected modules(or more, but usually two is enough, because it is less complicated to code and maintain)are working propperly.

## BVA

BVA is a top method for creating integration tests and blackbox tests, when using BVA you should take module(or few modules) and analyse inputs and theirs boudaries, and then pick values that are placed on the border of equivalence classes. To assure the correctness it is usually to chek one value in between of the boundaries just in case.


## Lab
Key for spring semester in 2022 is `AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w`

Ok, finally, we won't need to write any code today :). Hope you're happy)
1. Create your fork of the `
Lab5 - Integration testing
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab5-integration-testing)
2. Try to use InnoDrive application:
- At first we need to check our default parameters, for it we will need some REST client preferrably(Insomnia, Postman or you may use your browser), install it. 
- Then let's read through the description of our system:
    The InnoDrive is a hypothetical car sharing service that provides transportations means in Kazan region including Innopolis. The service proposes two types of cars `budget` and `luxury`. The service offers flexible two tariff plans. With the `minute` plan, the service charges its client by `time per minute of use`. The Service also proposes a `fixed_price` plan whereas the price is fixed at the reservation time when the route is chosen. If the driver deviates from the planned route for more than `n%`, the tariff plan automatically switches to the `minute` plan. The deviation is calculated as difference in the distance or duration from the originally planned route. The `fixed_price` plan is available for `budget` cars only. The Innopolis city sponsors the mobility and offers `m%` discount to Innopolis residents.
    Imagine you are the quality engineer of Innodrive. How are you going to know about application quality???
- InnoDrive application is just a web application that allows you to compute the price for a certain car ride
- To get particularly your defaults, let's send this request:
    `https://script.google.com/macros/s/_your_key_/exec?service=getSpec&email=_your_email_` 
  This will return our default spec(your result might be different):
        Here is InnoCar Specs:

        Budet car price per minute = 17

        Luxury car price per minute = 33

        Fixed price per km = 11

        Allowed deviations in % = 10

        Inno discount in % = 10

- To get the price for certain car ride, let's send next request:
`https://script.google.com/macros/s/_your_key_/exec?service=calculatePrice&email=_your_email_&type=_type_&plan=_plan_&distance=_distance_&planned_distance=_planned_distance_&time=_time_&planned_time=_planned_time_&inno_discount=_discount_` with some random parameters. Answer should be like `{"price": 100}` or `Invalid Request`
- And next we can create our BVA table, this is a lab, so I will create it just for 2 parameters: 
`distance` and `type`.
 `distance` is an integer value, so we can build 2 equivalence classes:
 + `distance` <= 0
 + `distance` > 0
 while `type` have 3 equivalence classes:
 + `budget` 
 + `luxury`
 + or some `nonsense`.

- Let's use BVA to make integration tests,we need to make few different testcases with `distance`, depending on its value: 
 + `distance` <= 0 : -10 0
 + `distance` > 0 : 1 100000
This way we will test both our borders and our normal values, we can do the same thing for `type`:
 + `type` = "budget" 
 + `type` = "luxury" 
 + `type` = "Guten morgen sonnenschein" 

- Now, let's check responses for different values of our parameters, with all other parameters already setted up, so we will have:
  + `plan` = `minute` 
  + `planned_distance` = `100` 
  + `time` = `110` 
  + `planned_time` = `100` 
  + `inno_discount` = `yes`
Let's build test cases out of this data:

| Test case  | distance |   type    | Expected result      |
|------------|----------|-----------|----------------------|
|     1      |    -10   |     *     |   Invalid Request    |
|     2      |    0     |     *     |   Invalid Request    |
|     3      |    *     | "nonsense"|   Invalid Request    |
|     4      |    1     |"budget"   |   1683               |
|     5      |    1     |"luxury"   |   3267               |
|     6      | 1000000  |"budget"   |   1683               |
|     7      | 1000000  |"luxury"   |   3267               |

etc...(there are a lot of combinations), to pick only needed one it is a good practice to use specialized tools.
Let's use Decision table to cut out our tests:
| Conditions(inputs)  |             Values           |    R1    |   R2    |   R3  |    R4   |
|---------------------|------------------------------|----------|---------|-------|---------|
|     Type            | "budget","luxury", nonsense  | nonsense |  budget | luxury|    *    |
|     Distance        | >0, <=0                      |     *    |  >0     |  >0   |    <=0  |
|---------------------|------------------------------|----------|---------|-------|---------|
|  Invalid Request    |                              |    X     |         |       |    X    |
|     200             |                              |          |    X    |   X   |         |

Now let's use this request to test our values




## Homework

As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the Readme of your branch. 



| Description | Value |
|-|-|
| Budget car price per minute | 22 |
| Luxury car price per minute | 55 |
| Fixed price per km | 18 |
| Allowed deviations in % | 20 |
| Inno discount in % | 6 |

At the beginning we need to define number of possible combinations of each parameter.

## BVA

| Variable Name | Variable Type | Possible Values | Number Of Possible Values |
| - | - | - | - |
| distance | integer (according to lab overview) | <= 0, > 0 | 2 |
| planned_distance | integer (according to distance) | <= 0, > 0 | 2 |
| time | not specified | <= 0, > 0 | 2 |
| planned_time | not specified | <= 0, > 0 | 2 |
| type | string | budget, luxury, nonsense | 3 |
| plan | string | minute, fixed_price, nonsense | 2 |
| inno_discount | boolean | yes, no, nonsense | 2 |

2 * 2 * 2 * 2 * 3 * 2 * 2 = 192 possible combinations.  
But we don't need to use them all. For example, we don't need to test abnormal values with other abnormal values. Based on provided data, we don't have provided upper boundaries of numeric variables, except of `distance` variable, that was mentioned in creating BVA table in lab description, but only based on it's type, we know limits of numbers that integer can handle.  

## Decision Table

Expected number of invalid requests in Decision Table is 9, one for each variable + two for `luxury` type restrictions. Since `fixed_price` is available only for `budget` cars.  
Expected number of `200 OK` request in Decision Table is 6, four for `budget` and two for `luxury`.  
`Distance` parameter cannot be < 0, but can be = 0 if taxi is cancelled. We have no additional information about it, hence we have only one condition - taxi always is being used and rides to end point of destination, distance can change and may be less or more than planned distance, but = 0 is impossible.  
`Planned distance` parameter cannot be <=0, because this state breaks whole logic of using taxi. Calling a taxi to ride 0 distance shouldn't be accepted as `OK`, it should interpreted as `Invalid request`. 
`Time` parameter is the same as with the `distance parameter`.  
`Planned time` parameter is the as with the `planned distance` parameter.  

`*` - any possible valid result


|  Conditions(inputs) |             Values                   |    R1    |    R2    |    R3    |    R4    |    R5    |    R6    |    R7    |    R8    |    R9    |     R10     |    R11      |   R12  |   R13  |     R14     |     R15     |
|---------------------|--------------------------------------|----------|----------|----------|----------|----------|----------|----------|----------|----------|-------------|-------------|--------|--------|-------------|-------------|
|     `distance`      |      `<=0, >0`                       |   `<=0`  |   `>0`   |   `>0`   |   `>0`   |   `>0`   |   `>0`   |   `>0`   |   `>0`   |   `>0`   |    `>0`     |    `>0`     |  `>0`  |  `>0`  |    `>0`     |    `>0`     |
|  `planned_distance` |      `<=0, >0`                       |     `*`  |   `<=0`  |   `>0`   |   `>0`   |   `>0`   |   `>0`   |   `>0`   |   `>0`   |   `>0`   |    `>0`     |    `>0`     |  `>0`  |  `>0`  |    `>0`     |    `>0`     |
|     `time`          |      `<=0, >0`                       |     `*`  |    `*`   |   `<=0`  |   `>0`   |   `>0`   |   `>0`   |   `>0`   |   `>0`   |   `>0`   |    `>0`     |    `>0`     |  `>0`  |  `>0`  |    `>0`     |    `>0`     |
|     `planned_time`  |      `<=0, >0`                       |     `*`  |    `*`   |    `*`   |   `<=0`  |   `>0`   |   `>0`   |   `>0`   |   `>0`   |   `>0`   |    `>0`     |    `>0`     |  `>0`  |  `>0`  |    `>0`     |    `>0`     |
|     `type`          |   `budget`, `luxury`, `nonsense`     |     `*`  |    `*`   |    `*`   |    `*`   |`nonsense`|    `*`   |    `*`   | `budget` | `budget` |  `budget`   |  `budget`   |`luxury`|`luxury`|  `luxury`   |  `luxury`   |
|     `plan`          |   `minute`, `fixed_price`, `nonsense`|     `*`  |    `*`   |    `*`   |    `*`   |    `*`   |`nonsense`|    `*`   | `minute` | `minute` |`fixed_price`|`fixed_price`|`minute`|`minute`|`fixed_price`|`fixed_price`|
|     `inno_discount` |   `yes`, `no`, `nonsense`            |     `*`  |    `*`   |    `*`   |    `*`   |    `*`   |    `*`   |`nonsense`|  `yes`   |   `no`   |    `yes`    |     `no`    | `yes`  |  `no`  |    `yes`    |    `no`     |
|     **Result**      |                                      |          |          |          |          |          |          |          |          |          |             |             |        |        |             |             |
| **Invalid Request** |                                      |   **X**  |   **X**  |   **X**  |   **X**  |   **X**  |   **X**  |   **X**  |          |          |             |             |        |        |    **X**    |    **X**    |
|     **200 OK**      |                                      |          |          |          |          |          |          |          |   **X**  |   **X**  |    **X**    |    **X**    |  **X** |  **X** |             |             |         |

## Test cases

| **Test case** | **Test scenario** | `distance` | `planned_distance` | `time` | `planned_time` |     `type`     |     `plan`      | `inno_discount` | **Expected result** | **Actual result**  |
|---------------|-------------------|------------|--------------------|--------|----------------|----------------|-----------------|-----------------|---------------------|--------------------|
|       1       |        R1         |    -10     |         1          |    1   |        1       |    `budget`    |    `minute`     |      `no`       |   Invalid Request   |   Invalid Request  |
|       2       |        R1         |     0      |         1          |    1   |        1       |    `budget`    |    `minute`     |      `no`       |   **Invalid Request**   |   **{"price":28.6}**|
|       3       |        R2         |     1      |        -10         |    1   |        1       |    `budget`    |    `minute`     |      `no`       |   Invalid Request   |   Invalid Request  |
|       4       |        R2         |     1      |         0          |    1   |        1       |    `budget`    |    `minute`     |      `no`       |   **Invalid Request**   |   **{"price":28.6}**   |
|       5       |        R3         |     1      |         1          |   -10  |        1       |    `budget`    |    `minute`     |      `no`       |   Invalid Request   |   Invalid Request  |
|       6       |        R3         |     1      |         1          |    0   |        1       |    `budget`    |    `minute`     |      `no`       |   **Invalid Request**   |   **{"price":0}**      |
|       7       |        R4         |     1      |         1          |    1   |       -10      |    `budget`    |    `minute`     |      `no`       |   Invalid Request   |   Invalid Request  |
|       8       |        R4         |     1      |         1          |    1   |        0       |    `budget`    |    `minute`     |      `no`       |   **Invalid Request**   |   **{"price":28.6}**   |
|       9       |        R5         |     1      |         1          |    1   |        1       |    `nonsense`  |    `minute`     |      `no`       |   Invalid Request   |   Invalid Request  |
|       10      |        R6         |     1      |         1          |    1   |        1       |    `budget`    |    `nonsense`   |      `no`       |   Invalid Request   |   Invalid Request  |
|       11      |        R7         |     1      |         1          |    1   |        1       |    `budget`    |    `minute`     |    `nonsense`   |   Invalid Request   |   Invalid Request  |
|       12      |        R8         |     1      |         1          |    1   |        1       |    `budget`    |    `minute`     |      `yes`      |   **{"price":20.68}**   |   **{"price":26.884}** |
|       13      |        R9         |     1      |         1          |    1   |        1       |    `budget`    |    `minute`     |      `no`       |   **{"price":22}**      |   **{"price":28.6}**   |
|       14      |        R10        |     1      |         1          |    1   |        1       |    `budget`    |  `fixed_price`  |      `yes`      |   **{"price":16.92}**   |   **{"price":11.75}**  |
|       15      |        R11        |     1      |         1          |    1   |        1       |    `budget`    |  `fixed_price`  |      `no`       |   **{"price":18}**      |   **{"price":12.5}**   |
|       16      |        R12        |     1      |         1          |    1   |        1       |    `luxury`    |    `minute`     |      `yes`      |   **{"price":51.7}**    |   **{"price":67.21}**  |
|       17      |        R13        |     1      |         1          |    1   |        1       |    `luxury`    |    `minute`     |      `no`       |   **{"price":55}**      |   **{"price":71.5}**   |
|       18      |        R14        |     1      |         1          |    1   |        1       |    `luxury`    |  `fixed_price`  |      `yes`      |   Invalid Request   |   Invalid Request  |
|       19      |        R15        |     1      |         1          |    1   |        1       |    `luxury`    |  `fixed_price`  |      `no`       |   Invalid Request   |   Invalid Request  |
|       20      |        R9         |     1      |         1          |  100000|        1       |    `budget`    |    `minute`     |      `no`       |   **{"price":2200000}**   |  **{"price":2860000}**    |
|       21      |        R8         |     1      |         1          |  100000|        1       |    `budget`    |    `minute`     |      `yes`      |   **{"price":2200000}**   |  **{"price":2688400}**    |
|       22      |        R11        |     100000 |         1          |    1   |        1       |    `budget`    |  `fixed_price`  |      `no`       |   **{"price":22}**  |  **{"price":16.666666666666668}** |
|       23      |        R10        |     100000 |         1          |    1   |        1       |    `budget`    |  `fixed_price`  |      `yes`      |   **{"price":20.68}**   |  **{"price":15.666666666666666}** |
|       24      |        R11        |     1      |         1          |  100000|        1       |    `budget`    |  `fixed_price`  |      `no`       |   **{"price":2200000}**   |   **{"price":166666.66666666666}**  |
|       25      |        R10        |     1      |         1          |  100000|        1       |    `budget`    |  `fixed_price`  |      `yes`      |   **{"price":2068000}**   |   **{"price":156666.66666666666}**  |
|       26      |        R13        |     1      |         1          |  100000|        1       |    `luxury`    |    `minute`     |      `no`       |   **{"price":5500000}**   |   **{"price":7150000}** |
|       27      |        R12        |     1      |         1          |    1   |        1       |    `luxury`    |    `minute`     |      `yes`      |   **{"price":5170000}**   |   **{"price":6721000}**  |


## Bugs
**Test cases: 2, 4, 6, 8**. Bad values validation, system must send Invalid Request.  
**Test case 12**. Bad calculations. One minute of budget car with discount must be 20.68 (22 per km - 6% discount) instead of 26.884.  
**Test case 13**. Bad calculations. One minute of budget car without discount must be 22 (22 per km) instead of 28.6.  
**Test case 14**. Bad calculations. Fixed price for budget car with discount must be 16.92 (18 per km - 6% discount) instead of 11.75.  
**Test case 15**. Bad calculations. Fixed price for budget car without discount must be 18 (18 per km) instead of 12.5.  
**Test case 16**. Bad calculations. Fixed price of luxury car with discount must be 51.7 (55 per km - 6% discount) instead of 67.21.  
**Test case 17**. Bad calculations. Fixed price of luxury car without discount must be 55 (55 per km) instead of 71.5.  
**Test case 20**. Bad calculations. 100000 minutes of budget car without discount must be 2200000 (22 per km) instead of 2860000.  
**Test case 21**. Bad calculations. 100000 minutes of budget car with discount must be 2068000 (22 per km - 6% discount) instead of 2688400.  
**Test case 22**. Bad calculations and deviations with distance. Fixed price for budget car without discount must change to minute plan and one minute of budget car without discount must be 22 (22 per km) instead of 16.666666666666668.  
**Test case 23**. Bad calculations and deviations with distance. Fixed price for budget car without discount must change to minute plan and one minute of budget car with discount must be 20.68 (22 per km - 6% discount) instead of 15.666666666666668.  
**Test case 24**. Same issue as in test case 22.  
**Test case 25**. Same issue as in test case 23.  
**Test case 26**. Same issue as in test case 17.  
**Test case 27**. Same issue as in test case 16.  
  
P.S. According to incorrect actual results, I can make a conclusion that `inno_discount` parameter works fine.

